/**
 * 
 */

/**
 * @author francis
 *
 */
public class T4_1 {

	/**
	 * Declara dos variables numéricas (con el valor que desees), muestra por consola la suma,
	resta, multiplicación, división y módulo (resto de la división).
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int v1 = 1;
		int v2 = 2;
		int s = v1 + v2;
		System.out.println(s);
		int r = v1 - v2;
		System.out.println(r);
		int m = v1 * v2;
		System.out.println(m);
		int md = v1 % v2;
		System.out.println(md);
		System.out.println("Proceso finalizado");
	}

}
